<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Competition extends Model
{

	public function subject()
	{
		return $this->hasOne('App\Subject','id','subject_id');
	}
}