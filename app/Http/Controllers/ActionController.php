<?php

namespace App\Http\Controllers;


Use App\Subject;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Session;

use App\Test;
use App\Ufb;
use App\Utf;
use App\User;
use Auth;
use Artisan;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use App\Book;
use App\Newmessage;
use App\Answer;
use App\Olimpiada;
use App\Osos;
use App\Message;
use App\Competition;
use Storage;
use App\Utfc;
use App\Gpgroup;
use App\Gpsch;
use App\Gplcenter;
use App\Groupmessage;

/**
* 
*/
class ActionController extends Controller
{
	public function getallolimps()
	{

    $this->olimpiadas = Olimpiada::all();


    return $this->olimpiadas;

	}

	public function actionIndex()
	{
		$subjects = Subject::all();
		//Artisan::call('Session:clear');
		return view($this->actionIndex,[
			'subjects'=>$subjects
		]);
	}


	public function actionSubject()
	{
		//dd(Session::get('subject_id'));
		$id = 0;
		if(is_null(Session::get('subject_id')))
			{
				$id = 1;
			}
		else
		{
			$id = Session::get('subject_id');
		}	
		// dd($id);
			$subject = Subject::Where('id',$id)->first();
		// dd($subject);
			$news = Newmessage::where('subject_id',$id)->orderBy('created_at','desc')->paginate(3);
			// dd($news);
		return view($this->actionSubject,[
			'subject'=>$subject,
			'news'=>$news,
		]);

	}

	public function actionTest()
	{
		
		if(is_null(Session::get('subject_id')))
			{
				$subject_id = 1;
			}
		else
		{
			$subject_id = Session::get('subject_id');
		}// dd($subject_id);
		// dd(Test::all());	
		$tests = Test::where('subject_id',$subject_id)->orderBy('created_at','desc')->paginate(13);
		 // dd($tests);
		$subject = Subject::where('id',$subject_id)->first();
		// dd($subject);
		return view($this->actionTest,[
			'tests' => $tests,
			// 'subject'=>$subject
		]);
	}

	public function actionReting()
	{
		
		if(is_null(Session::get('subject_id')))
			{
				$id = 1;
			}
		else
		{
			$id = Session::get('subject_id');
		}

		$ufbs = Ufb::where('subject_id',$id)->orderBy('ball','desc')->paginate(12);
		//$ufbs = orderBy('ball','desc');
		//dd($ufbs[10]->id);
		return view($this->actionReting,[
			'ufbs'=>$ufbs,
		]);
	}
	public function actionTestexam($id=null)
	{
		// dd($id);
		$test = Test::where('id',$id)->first();
		if(Auth::check())
		{
		
			 // dd($z);
			if(!is_null(Utf::where('user_id',Auth::user()->id)->where('test_id',$id)->first()))
			{
			$utf = Utf::where('test_id',$id)->where('user_id',Auth::user()->id)->first();
			 // dd($utf);
			return view($this->actionTestexam,[
				'utf'=>$utf,
				'true'=>'1'
			]);
			}
			else
			{
				// dd('samio');
				$utf = new Utf();
				$utf->user_id = Auth::user()->id;
				$utf->test_id = $id;
				$utf->count_answer = 0;
				$utf->ball = $test->ball;
				$utf->resoult = 2	;
				$utf->olimpiada_id = $test->olimpiada_id;
				$utf->gettime = Carbon::now()->toDateTimeString();
				$utf->save();
				 // $utf = Utf::where('user_id',Auth::user()->id)->where('test_id',$id)->first();
				 // dd($utf);
					return view($this->actionTestexam,[
					'utf'=>$utf,
					'true'=>'1'
				]);
			}
		}
		else
		{
			
			return view($this->actionTestexam,[
				'test'=>$test,
				'true'=>'0'
				]);
		}
	}
	public function back()
	{
		return redirect()->back();
	}
	public function actionStatus()
	{
		// $id = 0;
		if(is_null(Session::get('subject_id')))
		{
			$id = 1;
		}
		else
		{
			$id = Session::get('subject_id');
		}
		$tests = Test::where('subject_id',$id)->get();
		// dd($tests);
		// $utfs = Utf::all();
		$utfs = Utf::orderBy('gettime','desc')->paginate(13);
		// dd($utfs);

		return view($this->actionStatus,[
			'utfs'=>$utfs
		]);
	}

	public function actionUserstatus($id=null)
	{
		$iduser = $id;
		if(is_null(Session::get('subject_id')))
		{
			$sub_id = 1;
		}
		else
		{
			$sub_id = Session::get('subject_id');
		}
		$tests = Test::where('subject_id',$sub_id)->get();
		// dd($tests);
		// $utfs = Utf::all();
		 if(Auth::check() or Auth::user()->usertype->type == 'admin'){
		$utfs = Utf::where('user_id',$iduser)->orderBy('gettime','desc')->paginate(13);
		 // dd($utfs[12]);
			// $utfs = $utfs->where('user_id',Auth::user()->id)->paginate(13);
		return view($this->actionUserstatus,[
			'utfs'=>$utfs,
			'u'=>1,
			'iduser'=>$iduser

		]);
		}
		else{
		return view($this->actionUserstatus,[
			'u'=>0,
		]);	
		}
		// dd($utfs);

	}


		public function actionUserinfo($id=null,$reting=null)
	{
		$user = User::where('id',$id)->first();
		$messages = Message::where('user_2',$id)->where('condition','=','so\'rovda')->get();
		$count_messages = count($messages);
			$sub_id = 0;
		if(is_null(Session::get('subject_id')))
			{
				$sub_id = 1;
			}
		else
		{
			$sub_id = Session::get('subject_id');
		}
		if(Auth::check())
		{$ufb = Ufb::where('user_id',$id)->where('subject_id',$sub_id)->first();$k=1;}
		else
		{$ufb = Ufb::where('user_id',$id)->where('subject_id',$sub_id)->first();
		$k=0;}
		
		// dd($reting);
		$message = Groupmessage::where('user_id',$id)->where('answer',1)->first();
		
		return view($this->actionUserinfo,[
			'ufb'=>$ufb,
			't'=>0,
			'messagegroup'=>$message,
			'k'=>$k,
			'reting'=>$reting,
			'count_messages'=>$count_messages,
		]); 
	}


	public function actionOlimpiada($id=null)
	{
		// dd($id);
		$olimp = Olimpiada::where('id',$id)->first();
		// dd($olimp);
		$tests = Test::where('olimpiada_id',$id)->get();
        	$counttest = count($tests);
		$time = date("Y-m-d H:i:s",strtotime($olimp->begin_time) - strtotime('00:00') + strtotime($olimp->length));
        if (Carbon::now() > $time)
        	// dd('salom');
                return view($this->actionOlimpiada,
                	['messageFail' => 'Olimpiada yakunlangan '
                	,'counttest'=>$counttest,
                	'id'=>$id,
                	'time'=>$olimp->begin_time,
                	'status' => 0]);

        if (Carbon::now() < $olimp->begin_time)
            return view($this->actionOlimpiada,
            	['messageFail' => 'Olimpiada boshlanmagan',
            	'id'=>$id,'status' => 0,
            	'time'=>$olimp->begin_time,
            	'counttest'=>$counttest,]);
        // $end_time = $contest->end_time;
        if ($olimp->begin_time<Carbon::now() and $time>Carbon::now())
            // $time = $end_time;
        {
        	$m = date("H:i:s",strtotime($olimp->length)-strtotime('00:00')-(strtotime(Carbon::now())-strtotime('00:00')-strtotime($olimp->begin_time)));


        		// dd($m);

        return view($this->actionOlimpiada,
        	[
        		'tests' => $tests,
    			'id'=>$id,
    			'counttest'=>$counttest,
    			'status' => 1,
    			'm'=>$m	
    		]);
	    }
	}
	public function Resoultolimpiada($id=null)
	{
		$users = User::orderBy('created_at','desc')->paginate(13);
		// $utfs = Utf::where('olimpiada_id',$id)->paginate(13);
		// dd($sub);
		// $tests = Test::where('olimpiada_id',$id)->get();
	
		return view($this->actionResoultolimp,[
			'users'=>$users,
			'id'=>$id,
		]);
	}

	public function actionAllolimpiadas()
	{
		// $olimps = $this->getallolimps()->paginate(10);
		$olimps = Olimpiada::orderBy('created_at','desc')->paginate(13);
		// dd($olimps);
		return view($this->actionAllolimpiadas,[
			'olimps'=>$olimps,
		]);
	}
	public function allbooks()
	{
			$id = 0;
		if(is_null(Session::get('subject_id')))
			{
				$id = 1;
			}
		else
		{
			$id = Session::get('subject_id');
		}
		$books = Book::where('subject_id',$id)->get();

		return view($this->actionBooks,[
			'books'=>$books
		]);
	}
	public function viewNews($id=null)
	{
		$new = Newmessage::where('id',$id)->first();
		// dd($new);
		$new->count_views++;
		$new->save();
		return view($this->actionViewnew,[
			'new'=>$new
		]);
	}
	public function allanswers()
	{
		$answers = Answer::all();
		$array = Array();
		for($i=0;$i<count($answers);$i++)
		{
			array_push($array, $answers[$i]->test->id);
		}	
		// dd($array);
		$utfs = Utf::whereIn('test_id',$array)->get();

		if(Auth::check())
			$utfs = $utfs->where('user_id',Auth::user()->id);
		  // dd($utfs);

		return view($this->actionAnswer,[
			'utfs'=>$utfs,
		]);
	}
	public function addAnswerpanel($id=null)
	{
		$test = Test::where('id',$id)->first();
		return view('admin.addanswer',[
			'test'=>$test
		]);
	}
	public function addtestpanel()
	{
		$subjects = Subject::all();
		$olimpiadas = Olimpiada::all();

		return view($this->actionAddtest,[
			'subjects'=>$subjects,
			'olimpiadas'=>$olimpiadas,
		]);
	}
	public function addOlimpiadapanel()
	{
		$subjects = Subject::all();

		return view($this->actionOlimpiadapanel,[
			'subjects'=>$subjects,
		]);
	}
	public function addnewpanel()
	{
		$subjects = Subject::all();
		// $olimpiadas = Olimpiada::all();

		return view($this->actionaddnewpanel,[
			'subjects'=>$subjects,
		]);
	}
	public function addbookpanel()
	{
		$subjects = Subject::all();
		// $olimpiadas = Olimpiada::all();

		return view($this->actionaddbookpanel,[
			'subjects'=>$subjects,
		]);
	}
	public function OnlineUsers()
	{
		$users = User::where('id','>=',1)->paginate(13);

		return view($this->actionOnlineUsers,[
			'users'=>$users
		]);
	}
	public function Messages($id=null)
	{
		$messages = Message::where('user_2',$id)->where('condition','=','so\'rovda')->paginate(6);
		// dd($messages);
		return view($this->actionMessages,[
			'messages'=>$messages,
		]);
	}
	public function Competitionanswer($answer=null,$message=null)
	{
		$message = Message::where('id',$message)->first();
		if($answer==0)
		{
			$message->condition = 'rad etildi'; 
			$message->save();
			return redirect()->back();
		}
		else
		{
			$message->condition = 'qabul qilindi';
			$message->save();
			// dd($message);
			return redirect()->route('actionCompetitionTestget',[
				'message'=>$message->id,
			]);

		}
	}
	public function Competitionanswerfalse($message=null)
	{
		$message = Message::where('id',$message)->first();

			$message->condition = 'rad etildi'; 
			$message->save();
			return redirect()->route('ActionOnlineUsers');
	
	}
	public function Competitionanswertrue($message=null)
	{
		$message = Message::where('id',$message)->first();

			$message->condition = 'so\'rovda'; 
			$message->save();
			return redirect()->back()->with(['fail'=>'So\'rovingiz qaytadan jo\'natildi !!!']);
	
	}
	public function Competitiontestget($message=null)
	{

		// dd($message);
		$message = Message::where('id',$message)->first();
		// dd($message);

		$count_test = $message->competition->count_tests;
		// dd($count_test);
		$testId = Test::where('subject_id', '=',  $message->competition->subject_id)
			->inRandomOrder()
			->limit(36)        
			->take($count_test)
			->get();
			Storage::put('/competitions/'.$message->user_1.'/'.$message->competition->id.'.json', $testId);
			Storage::put('/competitions/'.$message->user_2.'/'.$message->competition->id.'.json', $testId);
			// $contents = Storage::get('file.json');

			// $array =  json_decode($contents);
		
			// dd($array);
		return redirect()->route('actionCompetitionTest',
			[
			'message'=>$message->id,
		]);

	}
	public function CompetitionTest($message=null)
	{
		$message = Message::where('id',$message)->first();
		$contents = Storage::get('/competitions/'.$message->user_1.'/'.$message->competition->id.'.json');
		$user = User::where('id',Auth::user()->id)->first();
		$tests =  json_decode($contents); 
		$count_test = count($tests);
		return view($this->actionCompetitionTest,[
			'user'=>$user,
			'tests'=>$tests,
			'counttest'=>$count_test,
			'comp'=>$message->competition_id,
			'message'=>$message->id,

		]);
	}
	public function Waitpanel($message=null)
	{
		
		$message = Message::where('id',$message)->first();

		return view($this->actionWaitpanel,[
			'message'=>$message,
		]);
	}
	public function Competitionexam($id=null,$comp=null)
	{
		$test = Test::where('id',$id)->first();
// dd($comp);
			$z=Utfc::where('user_id',Auth::user()->id)->where('competition_id',$comp)->where('test_id',$id)->first();
			  // dd($z);
			if($z)
			{
			$utfc = Utfc::where('competition_id',$comp)->where('user_id',Auth::user()->id)->where('test_id',$id)->first();
			  // dd($utfc);
			return view($this->actionCompetitionexam,[
				'utfc'=>$utfc,
			]);
			}
			else
			{
				$utfc = new Utfc();
				$utfc->user_id = Auth::user()->id;
				$utfc->test_id = $id;
				$utfc->count_answer = 0;
				$utfc->ball = $test->ball;
				$utfc->resoult = 0;
				$utfc->competition_id = $comp;
				$utfc->gettime = Carbon::now()->toDateTimeString();
				// dd($utfc);
				$utfc->save();
				 // $utfc = utfc::where('user_id',Auth::user()->id)->where('test_id',$id)->first();
				 // dd($utfc);
					return view($this->actionCompetitionexam,[
					'utfc'=>$utfc,
				]);
			}
	}
	public function CompStatus($comp=null,$message=null)
	{
			  // dd($comp);
		$utfcs = Utfc::where('competition_id',$comp)->where('user_id',Auth::user()->id)->orderBy('gettime','desc')->paginate(13);
		// dd($utfcs);
		return view($this->actionCompetitionuserstatus,[
			'utfcs'=>$utfcs,
			'comp'=>$comp,
			'message'=>$message,

		]);
	}
	public function allCompetition($id=null)
	{
		$messages = Message::where('user_1',$id)->orWhere('user_2',$id);
		// dd($messages);
		$messages = $messages->where('condition','=','qabul qilindi')->orderBy('created_at','desc')->paginate(13);
		//dd($messages);
		return view($this->actionallCompetitionuser,[
			'messages'=>$messages,
		]);
	}
	public function grouplist()
	{
		$gpgroups = Gpgroup::orderBy('reyting','desc')->get();
		$gpschs = Gpsch::all();
		$gplcenrets = Gplcenter::all();

		return view($this->actionGroupslist,[
			'gpgroups'=>$gpgroups,
		]);
	}
	public function actionGpgroup()
	{
		$subjects = Subject::all();
		return view($this->actionGpgroup,[
				'subjects'=>$subjects,
		]);
	}
	public function actionGroupinfo($id=null)
	{
		$group = Gpgroup::where('id',$id)->first();
		$users = User::where('gpgroup_id',$id)->get();
		// foreach($users as $user)
		$count_messages = count(Groupmessage::where('group_id',$id)->where('answer',-1)->get());
		// $count = count($messages);
		// dd($count_messages);
		return view($this->actionGroupinfo,[
			'group'=>$group,
			'users'=>$users,
			'count_messages'=>$count_messages,
		]);
	}
	public function actionGroupmessages($id=null)
	{
		$messages = Groupmessage::where('group_id',$id)->where('answer',-1)->paginate(13);
		// dd($messages[0]);

		return view($this->actionGroupmessages,[
			'messages'=>$messages,
		]);
	}
}

