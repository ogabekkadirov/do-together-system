<?php

namespace App\Http\Controllers;


Use App\Subject;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Session;

use App\Test;
use App\Ufb;
use App\Utf;
use App\User;
use Auth;
use Artisan;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use App\Book;
use App\Newmessage;
use App\Answer;
use App\Olimpiada;
use App\Osos;
use App\Message;
use App\Competition;
use Storage;
use App\Utfc;
use App\Usertype;
/**
 * 
 */
class AdminactionController extends Controller
{
	public function index()
	{
		return view($this->actionAdminIndex);
	}


	public function users()
	{
		$users = User::all();
		// dd($user[1]->usertype->name);
		$usertypes = Usertype::all();
		return view($this->actionAdminUsers,[
			'users'=>$users,
			'usertypes'=>$usertypes
		]);
	}
	public function adminalltests()
	{
		$tests = Test::all();
		return view($this->actionAdminTests,[
			'tests'=>$tests
		]);
	}
	public function login()
	{
		return view($this->actionlogin);
	}
	public function changetest($id=null)
	{
		$subjects = Subject::all();
		$olimpiadas = Olimpiada::all();
		$test = Test::where('id',$id)->first();
		return view($this->actionEditTests,[
			'subjects'=>$subjects,
			'olimpiadas'=>$olimpiadas,
			'test'=>$test
		]);
	}
	public function changenewpanel($id=null)
	{
		$subjects = Subject::all();

		$new = Newmessage::where('id',$id)->first();
		return view($this->actionChangenew,[
			'subjects'=>$subjects,
			'new'=>$new
		]);
	}
	public function deletetest($id=null)
	{
		$test = Test::find($id);
		// dd($test->id);
		$utfs = Utf::where('test_id',$id);
		 // dd($utfs);
		
		$test->delete();
		$utfs->delete();
		return redirect()->back()->with(['fail'=>'Ma\'lumot o\'chirildi !!!']);
	}
	public function adminallnews()
	{
		$news = Newmessage::all();
		return view($this->actionallTests,[
			'news'=>$news
		]);
	}
		public function Changenewid(Request $request)
	{
		// dd('salom');
		$subject = Subject::where('name',$request['subject'])->first();
		$new = Newmessage::where('id',$request['id'])->first();
		$new->theme = $request['theme'];
		$new->info = $request['info'];
		$new->subject_id =$subject->id;
		$new->count_views = 0;
		$new->autor = $request['autor'];
		$new->time = Carbon::now()->toDateTimeString();
		if($request->hasFile('image'))
		{
			$file = Input::file('image');

			$image_id = ImageController::setimage($file,0);

			$new->image_id = $image_id; 

		}
		// dd($new);
		$new->save();
		return redirect()->route('adminallnews')->with(['fail'=>'Ma\'lumotlar o\'zgartirildi !!!']);
	}
	// fdsfdsfsd
	public function deletenewid($id=null)
	{
		$new = Newmessage::where('id',$id)->first();
		$new->delete();
		return redirect()->back()->with(['fail'=>'M\'alumotlar o\'chirildi !!!']);
		
	}

	public function deleteuser($id=null)
	{
		$user = User::where('id',$id)->first();
		$user->delete();
		$ufbs = Ufb::where('user_id',$id);
		$utfs = Utf::where('user_id',$id);
		$utfs->delete();
		$ufbs->delete();
		return redirect()->back()->with(['fail'=>'M\'alumotlar o\'chirildi !!!']);
		
	}
	public function Changeuser($id=null)
	{
		$user = User::find($id)->first();
		$usertypes = Usertype::all();
		return view($this->actionChangeuser,[
			'user'=>$user,
			'usertypes'=>$usertypes

		]);
	}

	public function getUserchange(Request $request)
	{

		$this->validate($request,[
			// 'password' => 'required|max:15',
			'firstname' => 'required|max:16',
			'lastname' => 'required|max:16',
			'tel_number' => 'required|max:13',
		]);
		
		$a = $request['tel_number'];
		// dd(strlen($a));
		$z=0;
		for($i=1;$i<strlen($a);$i++)
		{
			// dd($a[$i]);
			if($a[$i]<'0' or $a[$i]>'9')
			{
				$z=1;break;
			}
		}
				// dd('salom');

		// dd($z);
		if($request['firstname']=='' or $request['lastname']=='' or $request['login']=='' or $request['password']=='' or $z)
		{
				// dd('salom');
				return redirect()->back()->with([ 'fail' =>'Ma\'lumotlar to\'liq kiritilmagan yoki xato kiritilgan!!!']);

		}

		else
			{
				
				if($request['password']!='')
				{
					
					$a = $request['tel_number'];
					$user = User::where('id',$request['id'])->first();
					$user->firstname = $request['firstname'];
					$user->lastname = $request['lastname'];
					$user->login = $request['login'];
					$user->password = bcrypt($request['password']);
					$user->tel_number = $a;
					$user->type = $request['type'];
					$user->country = $request['country'];
					$user->online_ball = 0;
					$user->image_id = 21;
					$user->usertype_id = Usertype::where('type',$request
						['usertype'])->first()->id;
					$user->save();
					// $this->creatuser($user->id);
					return redirect()->route('AdminUsers')->with(['fail'=>'M\'alumotlar o\'zgartirildi !!!']);

				}
				else
				{

					return redirect()->back()->with(['fail' => 'Parolni qayta kiritishda xatolik bor. Qaytadan urinib ko\'ring!!! ']);
				}
			
			}

	}
	public function changeusertype(Request $request)
	{
		$id = $request['id'];
		$user = User::where('id',$id)->first();
		$user->usertype_id = Usertype::where('type',$request['usertype'])->first()->id;
		$user->save();
		return redirect()->back()->with(['fail'=>'M\'alumotlar o\'zgartirildi !!!']);
	}
}