<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

class Controller extends BaseController
{
	public $actionIndex = 'firstindex';
    public $actionSubject = 'subjectsnew';
    public $actionTest = 'subjecttests';
    public $actionReting = 'usersreting';
    public $actionTestexam = 'testexam1';
    public $actionStatus = 'allstatuspage';
    public $actionUserinfo = 'usersinfo';
    public $actionUserstatus = 'userstatus';
    public $actionOlimpiada = 'olimpiada';
    public $actionAllolimpiadas = 'allolimpiadas';
    public $actionBooks = 'bookssubject';
    public $actionViewnew = 'viewnew';
    public $actionAnswer = 'answers';
    public $actionAddtest = 'admin.addtestadmin';
    public $actionOlimpiadapanel = 'admin.addolimpiada';
    public $actionaddnewpanel  = 'admin.addnew';
    public $actionaddbookpanel  = 'admin.addbook';
    public $actionResoultolimp = 'resoultolimpiada';
    public $actionUsertouserpanel = 'usertouser';
    public $actionOnlineUsers = 'onlineusers';
    public $actionWaitpanel = 'waitpanel';
    public $actionMessages = 'usermessage'; 
    public $actionCompetitionTest = 'Competitiontest';
    public $actionCompetitionexam = 'Competitionexam';
    public $actionCompetitionuserstatus = 'compuserstatus';
    public $actionCompetitionresoult = 'resoultCompetition';
    public $actionallCompetitionuser = 'allcompetitionuser';
    public $actionAdminIndex = 'admin.index';
    public $actionAdminUsers = 'admin.users';
    public $actionlogin = 'admin.login';
    public $actionAdminTests = 'admin.alltests';
    public $actionEditTests = 'admin.edittest';
    public $actionallTests = 'admin.allnews';
    public $actionChangenew = 'admin.changenew';

    public $actionChangeuser = 'admin.changeuserinfo';
    public $actionGroupslist = 'groupslist';
    public $actionGpgroup = 'registrationgroup';
    public $actionGroupinfo = 'infogroup';
    public $actionGroupmessages = 'groupmessage';
    
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

}
