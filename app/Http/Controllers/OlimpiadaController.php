<?php

namespace App\Http\Controllers;


Use App\Subject;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use App\Test;
use App\Ufb;
use App\Utf;
use App\User;
use Auth;
use Artisan;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use App\Olimpiada;

/**
* 
*/
class OlimpiadaController extends Controller
{
	public function addOlimpiada(Request $request)
	{
		$olimpiada = new Olimpiada();
		$olimpiada->name  = $request['name'];
		$olimpiada->begin_time = $request['b_time'];
		$olimpiada->length = $request['length'];
		$olimpiada->subject_id = Subject::where('name',$request['subject'])->first()->id;
		// dd($olimpiada);
		$olimpiada->save();
		return redirect()->route('actionAddtest');
	}

}