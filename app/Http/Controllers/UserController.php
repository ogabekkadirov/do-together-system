<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use App\Ufb;
use App\Utf;
use App\Subject;
use Cache;
use Session;
use Artisan;
use Carbon\Carbon;
use App\Image;
use Illuminate\Support\Facades\Input;
use App\Message;
use App\Compatition;
use App\Test;
/**
* 
*/
class UserController extends Controller
{
	static function creatuser($id=null)
	{
		for($i = 1; $i <=9; $i++)
		{
			$ufb = new Ufb();
			$ufb->user_id = $id;
			$ufb->subject_id = $i;
			$ufb->ball = 0;
			$ufb->count_answer = 0;
			$ufb->count_true_answer = 0;
			$ufb->lasttime = Carbon::now()->toDateTimeString();
			// dd($fub);	
			$ufb->save();

		}
		return true;
	}
	
	public function setuser(Request $request)
	{

		$this->validate($request,[
			// 'password' => 'required|max:15',
			'firstname' => 'required|max:16',
			'lastname' => 'required|max:16',
			'tel_number' => 'required|max:9',
		]);
		
		$a = $request['tel_number'];
		// dd(strlen($a));
		$z=0;
		for($i=0;$i<strlen($a);$i++)
		{
			// dd($a[$i]);
			if($a[$i]<'0' or $a[$i]>'9')
			{
				$z=1;break;
			}
		}
		if(is_null(User::Where('login',$request['login'])->first()))
		{

		// dd($z);
		if($request['firstname']=='' or $request['lastname']=='' or $request['login']=='' or $request['password']=='' or $z)
		{
				
				return redirect()->back()->with([ 'fail' =>'Ma\'lumotlar to\'liq kiritilmagan yoki xato kiritilgan!!!']);

		}

		else
			{
				
				if($request['password']==$request['conformpassword'])
				{
					
					$a = '+9989'.$request['tel_number'];
					$user = new User();
					$user->firstname = $request['firstname'];
					$user->lastname = $request['lastname'];
					$user->login = $request['login'];
					$user->password = bcrypt($request['password']);
					$user->tel_number = $a;
					$user->type = $request['type'];
					$user->country = $request['country'];
					$user->online_ball = 0;
					$user->image_id = 1;
					$user->usertype_id = 1;
					$user->gpgroup_id =1;
					$user->gpsch_id =1;
					$user->gplcenter_id =1;
					$user->save();
					$this->creatuser($user->id);

				}
				else
				{
					return redirect()->back()->with(['fail' => 'Parolni qayta kiritishda xatolik bor. Qaytadan urinib ko\'ring!!! ']);
				}
			}


		if(!Auth::attempt(['login' => $request['login'], 'password' => $request['password']]))
        {
            return redirect()->back()->with(['fail' => 'Parol yoki login xato yoki ular mavhud boshqa login va parol kiriting !!! ']);
        }
        else
        {
    		return redirect()->route('actionSubject');
    	}
    }
    else{
    	return redirect()->back()->with(['fail' => 'Bunday loginli odam ro\'yhatdan o\'tgan !!! Iltimos boshqa login kiriting. ']);
    }

	}
	public function logout()
	{
		if(Auth::check())
			{
				$ufb = ufb::where('user_id',Auth::user()->id)->first();
				// dd(Auth::user()->id);
				$ufb->lasttime = Carbon::now()->toDateTimeString();
				
				$ufb->save();
				Auth::logout();
				// Artisan::call('Session:clear');
				// dd('salom');
				return redirect()->route('actionIndex');
			}
	}
	public function enter(Request $request)
	{
			$sub_id = 0;
		if(is_null(Session::get('subject_id')))
			{
				$sub_id = 1;
			}
		else
		{
			$sub_id = Session::get('subject_id');
		}
		
		if(Auth::attempt(['login' => $request['login'], 'password' => $request['password']]) && Auth::user()->usertype->type == 'admin')
		{
        	return redirect()->route('AdminIndex');

		}
		else
        {
        	if(!Auth::attempt(['login' => $request['login'], 'password' => $request['password']]))
        	{
            return redirect()->back()->with(['fail' => 'Parol yoki login noto\'g\'ri kiritilgan !!! ']);
        }
        else
        {
        	// $subject = Subject::where('id',$request['subject'])->first();
        	return redirect()->route('actionSubject');
        }
    }
	}
	public function userimage(Request $request)
	{
		$user = User::where('id',Auth::user()->id)->first();
		if($request->hasFile('image'))
		{	
			$file = Input::file('image');
			
			$image_id = ImageController::setimage($file,$user->image_id);

			$user->image_id = $image_id;

		}
			// dd($user->image->path);

		$user->save();
			$sub_id = 0;
		if(is_null(Session::get('subject_id')))
			{
				$sub_id = 1;
			}
		else
		{
			$sub_id = Session::get('subject_id');
		}
		$ufb = Ufb::where('user_id',$user->id)->where('subject_id',$sub_id)->first();
		$messages = Message::where('user_2',Auth::user()->id)->where('condition','=','so\'rovda')->get();
		$count_messages  = count($messages);
		return view($this->actionUserinfo,[
			'ufb'=>$ufb,
			't'=>0,
			'reting'=>$request['reting'],
			'count_messages'=>$count_messages,

		]);
	}
	public function truetests($id=null,$n=null)
	{

		
			$sub_id = 0;
		if(is_null(Session::get('subject_id')))
			{
				$sub_id = 1;
			}
		else
		{
			$sub_id = Session::get('subject_id');
		}
		$tests = Test::where('subject_id',$sub_id)->get();

		$array = Array();
		for($i=0;$i<count($tests);$i++)
		{
			array_push($array, $tests[$i]->id);
		}
		

		$ufb = Ufb::where('user_id',$id)->where('subject_id',$sub_id)->first();
		$utfs =  Utf::where('user_id',$id)->Where('resoult',1)->get();
		 // dd($utfs);

		// $utfs = $utfs->where('user_id',$id)->get();
		//dd($ufb);

		// dd($n);
		$messages  = Message::where('user_2',$id)->where('condition','=','so\'rovda')->get();
		//dd($messages);
		$count_messages  = count($messages);
		return view($this->actionUserinfo,[
			'utfs'=>$utfs,
			't'=>1,
			'ufb'=>$ufb,
			'reting'=>$n,
			'count_messages'=>$count_messages,

		]);
	}

	public function searchUser(Request $request)
	{
		$res ='';
		$res1='';
		$searchuser = $request['searchuser'];
		for($i=0;$i<strlen($searchuser);$i++){
				if($i == 0){
					$res .= strtoupper($searchuser[$i]);
					$res1 .= strtolower($searchuser[$i]);
				}
				else{
					$res .= $searchuser[$i];
					$res1 .= $searchuser[$i];
				}

			}
		$users = User::where('firstname','LIKE','%' .$res. '%' )->orWhere('lastname','LIKE','%' .$res. '%')->orWhere('firstname','LIKE','%' .$res1. '%' )->orWhere('lastname','LIKE','%' .$res1. '%')->get();
		// dd()
		$array = Array();
		for($i=0;$i<count($users);$i++)
		{
			array_push($array, $users[$i]->id);
		}
			$sub_id = 0;
		if(is_null(Session::get('subject_id')))
			{
				$sub_id = 1;
			}
		else
		{
			$sub_id = Session::get('subject_id');
		}
		// dd($array);
		$ufbs = Ufb::whereIn('user_id',$array)->where('subject_id',$sub_id)->paginate(13);
		// $
		// dd($ufbs);

		return view($this->actionReting,[
			'ufbs'=>$ufbs,
		]);
	}
	public function usertouserpanel($id=null){
		$subjects = Subject::all();
 		$user2 = User::where('id',$id)->first();
 		$user1 = User::where('id',Auth::user()->id)->first();
		return view($this->actionUsertouserpanel,[
			'subjects'=>$subjects,
			'user1' =>$user1,
			'user2' =>$user2
		]);	
	}

}

