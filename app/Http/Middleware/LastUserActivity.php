<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Cache;
use Carbon\Carbon;

/**
* 
*/
class LastUserActivity 
{
	public function handle($request,Closure $next)
	{
		if(Auth::check()){

			$expriesAt = Carbon::now()->addMinutes(60);
			Cache::put('user-is-online-'.Auth::user()->id,true,$expriesAt);
		}
		return $next($request);
	}
}