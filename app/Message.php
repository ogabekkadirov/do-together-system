<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
	public function competition()
	{
		return $this->hasOne('App\Competition','id','competition_id');        
	}

	public function user_1()
	{
		return $this->hasOne('App\User','id','user_1');
	}

	public function user_2()
	{
		return $this->hasOne('App\User','id','user_2');
	}
}
