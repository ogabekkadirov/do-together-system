<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Newmessage extends Model
{
   public function image()
    {
        return $this->hasOne('App\Image','id','image_id');        
    }
    public function subject()
    {
    	return $this->hasOne('App\Subject','id','subject_id');
    }
}
