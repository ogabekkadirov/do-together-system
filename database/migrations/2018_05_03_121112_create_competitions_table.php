<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompetitionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('competitions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_1');
            $table->integer('user_1_ball');
            $table->integer('user_2');
            $table->integer('user_2_ball');
            $table->datetime('begin_time');
            $table->time('length');
            $table->integer('count_tests');
            $table->integer('subject_id');
            $table->integer('prize');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('competitions');
    }
}
