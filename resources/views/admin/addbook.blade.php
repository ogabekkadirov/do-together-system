@extends('layout.main')

@section('header')
@include('includes.header1')
@section('header')

@section('content')

<div class="mainnews" >
	<div class="searchcontent">
		<div class="retingsearch">
			<p>Kitob qo'shish</p>
		</div>
		<div class="search">
			<div class="inputb">

			</div>
		</div>
	</div>
	<div class="table" style="background-color: #fff">
		<form action="{{ route('addBook') }}" method="post" enctype="multipart/form-data">
			<div class="addtema">
				<span>Fan : </span>
				<select class="subjecttest" name="subject">
					<!-- <option>Matematika</option> -->
					@foreach($subjects as $subject)
						<option>{{ $subject->name }}</option>
					@endforeach
				</select>
			</div>
			<div class="addtema">
				<span>Nomi : </span>
				<input type="text" name="name">
			</div>
			<div class="addtema">
				<!-- <span> : </span> -->
				<input type="file" name="book">
			</div>
			{!! csrf_field() !!}
			<div class="exam3">
				<button type="submit" style="max-height: 60px;">
					<i class="fa fa-plus"></i>Qo'shish
				</button>
			</div>
		</form>
	</div>
</div>

@endsection('content')

@section('menyu')
@include('includes.mainmenyu')
@endsection('menyu')