  @extends('layout.adminmain')

  @section('header')
  @include('includes.admin.header')
  @endsection('header')


  @section('content')
  <div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Hamma yangiliklar</h3> </div>

            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Jadvallar</a></li>
                    <li class="breadcrumb-item active">Yangililar</li>
                </ol>
            </div>
        </div>
        <!-- End Bread crumb -->
        <!-- Container fluid  -->
        <div class="container-fluid">
            <!-- Start Page Content -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                               @if(Session::has('fail'))
                                  <h6 style="color: green;">{{ Session::get('fail') }}</h6>
                               @endif
                        <div class="card-body">
                            <div class="table-responsive m-t-40">
                                <table id="table_id" class="display" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th> 
                                              №
                                            </th>
                                            <th> 
                                                Mavzusi 
                                            </th>
                                            <th>
                                               Fani
                                            </th>
                                            <th>
                                                Autori
                                            </th>
                                            <th>
                                               Ko'rishlar
                                            </th>
                                           
                                            <th>
                                                Amallar
                                            </th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        $number=1;
                                        ?>
                                        @foreach($news as $new)
                                        <tr>
                                            <td>{!! $number++ !!}</td>
                                            <td>
                                                <a href="#" style="float: left;">
                                                   {{ str_limit($new->theme,40) }}
                                                </a>
                                            
                                            </td>
                                            <td>{!! $new->subject->name !!}</td>
                                            <td>{!! $new->autor!!}</td>
                                            <td>{{ $new->count_views }}</td>
                                            <td>
                                                <a href="" title="delete" data-toggle="modal" data-target="#myModal{{$new->id}}"> <i class="fa fa-trash icon11"></i></a>
                                                <a href="{{ route('actionchangenew',['id'=>$new->id]) }}" title="change"> <i class="fa fa-edit icon12"></i></a>
                                                <a href="#" title="Add answer"> <i class="fa fa-plus-circle icon13"></i></a>
                                            </td>
                                        </tr>
                          <div class="modal fade" id="myModal{{$new->id}}" role="dialog">
                            <div class="modal-dialog modal-lg">
                              <div class="modal-content">
                                <div class="modal-header"> 
                                  <h4 class="modal-title">Ma'lumotni o'chirish</h4>
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body">
                                  <p class="delete">Siz haqiqatdan ham ma'lumotni o'chirmoqchimisiz !!!</p>

                                </div>
                                <div class="modal-footer">
                                  <a href="{{ route('deletenewid',['id'=>$new->id]) }}" class="btn btn-primary" >Ha</a>
                                  <button type="button" class="btn btn-default btn-danger" data-dismiss="modal">yo'q</button>

                                </div>
                              </div>
                            </div>
                          </div>
                                  @endforeach

                              </tbody>
                          </table>

                      </div>
                  </div>
              </div>
          </div>
      </div>


      @endsection('content')
      @section('script')

      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
      <script type="text/javascript">
        $(document).ready( function () {
            $('#table_id').DataTable();
        } );
    </script>

    @endsection('script')