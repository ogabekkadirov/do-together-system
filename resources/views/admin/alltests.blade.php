  @extends('layout.adminmain')

  @section('header')
  @include('includes.admin.header')
  @endsection('header')


  @section('content')
  <div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Testlar</h3> </div>

            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Jadvallar</a></li>
                    <li class="breadcrumb-item active">Testlar</li>
                </ol>
            </div>
        </div>
        <!-- End Bread crumb -->
        <!-- Container fluid  -->
        <div class="container-fluid">
            <!-- Start Page Content -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                                    @if(Session::has('fail'))
                                <h6 style="color: green;">{{ Session::get('fail') }}</h6>
                                @endif
                        <div class="card-body">
                            <div class="table-responsive m-t-40">
                                <table id="table_id" class="display" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th> 
                                                <i class="fa fa-list-ol"></i>№
                                            </th>
                                            <th> 
                                                <i class="fa fa-edit"></i> Nomi 
                                            </th>
                                            <th>
                                                <i class="fa fa-book"></i> Mavzusi 
                                            </th>
                                            <th>
                                                <i class="fa fa-balance-scale"></i> Ball 
                                            </th>
                                            <th>
                                                <i class="fa fa-check-circle"></i> To'gri javoblar soni
                                            </th>
                                            <th>
                                                <i class="fa fa-book"></i>Fan
                                            </th>
                                            <th>
                                                Amallar
                                            </th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        $number=1;
                                        ?>
                                        @foreach($tests as $test)
                                        <tr>
                                            <td>{!! $number++ !!}</td>
                                            <td>
                                                <a href="{{ route('actionTestexam',['id'=> $test->id ]) }}" style="float: left;">
                                                    {!! $test->name_test !!}
                                                </a>
                                                @if($test->have_answer)
                                                <i class="fa fa-check-circle" style="float: right;color: #62A1D8"></i>
                                                @endif
                                            </td>
                                            <td>{!! $test->theme !!}</td>
                                            <td>{!! $test->ball !!}</td>
                                            <td>{!! $test->true_answers !!}</td>
                                            <td>{{ $test->subject->name }}</td>
                                            <td>
                                                <a href="" title="delete" data-toggle="modal" data-target="#myModal{{$test->id}}"> <i class="fa fa-trash icon11"></i></a>
                                                <a href="{{ route('adminchangetest',['id'=>$test->id ]) }}" title="change"> <i class="fa fa-edit icon12"></i></a>
                                                <a href="{{ route('addAnswerpanel',['id'=>$test->id]) }}" title="Add answer"> <i class="fa fa-plus-circle icon13"></i></a>
                                            </td>
                                        </tr>
                                          <div class="modal fade" id="myModal{{$test->id}}" role="dialog">
                                            <div class="modal-dialog modal-lg">
                                              <div class="modal-content">
                                                <div class="modal-header"> 
                                                  <h4 class="modal-title">Ma'lumotni o'chirish</h4>
                                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                              </div>
                                              <div class="modal-body">
                                                  <p class="delete">Siz haqiqatdan ham ma'lumotni o'chirmoqchimisiz !!!</p>

                                              </div>
                                              <div class="modal-footer">
                                                <a href="{{ route('deletetest',['id'=>$test->id ]) }}" class="btn btn-primary" >Ha</a>
                                                  <button type="button" class="btn btn-default btn-danger" data-dismiss="modal">yo'q</button>
                                                  
                                              </div>
                                          </div>
                                      </div>
                                  </div>

                                  @endforeach

                              </tbody>
                          </table>

                      </div>
                  </div>
              </div>
          </div>
      </div>


      @endsection('content')
      @section('script')

      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
      <script type="text/javascript">
        $(document).ready( function () {
            $('#table_id').DataTable();
        } );
    </script>

    @endsection('script')