@extends('layout.adminmain')



@section('content')

        <div class="unix-login">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-lg-4">
                        <div class="login-content card">
                            <div class="login-form">
                            <div class="text-center"><img src="{{ URL::to ('src/')}}/img/logo/innew.png" style="width:90px"></div>
                                <h4><i class="fa fa-user"></i> Kirish</h4>
                                <form action="{{ route('enter') }}" method="post">
                                	{!! csrf_field() !!}
                                    <div class="form-group">
                                        <label>Login</label>
                                        <input type="text" class="form-control" placeholder="Login" name="login">
                                    </div>
                                    <div class="form-group">
                                        <label>Parol</label>
                                        <input type="password" class="form-control" placeholder="Parol" name="password">
                                    </div>
                                    <div class="checkbox">
                                        <label>
        										<input type="checkbox"> Remember Me
        									</label>
                                        <label class="pull-right">
        										<a href="#">Parolni unitdingizmi?</a>
        									</label>
      										@if(Session::has('fail'))
								<h6 style="color: red;">{{ Session::get('fail') }}</h6>
								@endif
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-flat m-b-30 m-t-30">Kirish</button>
                                    <div class="register-link m-t-15 text-center">
                                        <p>Hali ro'yxatdan o'tmaganmisiz? <a href="{{ route('registration') }}"> Ro'yxatdan o'rish</a></p>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection('content')