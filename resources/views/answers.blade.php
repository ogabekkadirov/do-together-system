@extends('layout.main')

@section('header')
@include('includes.header1')
@endsection('header')

@section('content')

<div class="mainnews">
	<div class="searchcontent">
		<div class="retingsearch">
			<p>Fanlar bo'yicha mavjud testlarning javoblari </p>
		</div>
		<div class="search">
			<div class="inputb">
			</div>
		</div>
	</div>
	@if(Auth::check())
	<div class="table">
		<table class="table table-striped table-bordered table-hover ">	
			<thead>
				<tr>
					<th> 
						<i class="fa fa-list-ol"></i>№
					</th>
					<th> 
						<i class="fa fa-edit"></i> Test nomi 
					</th>
					<th><i class="fa fa-arrow-circle-down"></i>Yuklashga ruxsat</th>
				</tr>
			</thead>
			<tbody>	
				<?php $number = 1; ?>
				@foreach($utfs as $utf)				   
				<tr>
					<td>{!! $number++ !!}</td>
					<td><a href="{{ $utf->test->id }}">{!! $utf->test->name_test !!}</a></td>
					@if($utf->count_answer==2 or $utf->resoult==1)
					<td>
						<a href="{{ $utf->id}}">
							<img src="{{ URL::to('src/img/true.png') }}"/>
						</a>
					</td>
					@else
					<td>
						<a href="{{ $utf->test->answer->path }}">
							<img src="{{ URL::to('src/img/red.png') }}"/>
						</a>
					</td>
					@endif
				</tr>	
				@endforeach
			</tbody>
		</table>
	</div>
	<div class="searchcontent">
		<div class="retingsearch">
			<p>
				<i class="fa fa-arrow-circle-down"></i> - Test javoblarini yuklab olish uchun siz bu testlarga javob yo'llab bo'lgan bo'lishingiz lozim !!! 
			</p>
		</div>
		<div class="search">
			<div class="inputb">
			</div>
		</div>
	</div>
	@else
	<div class="searchcontent">
		<div class="retingsearch">
			<p><i class="fa fa-question-circle"></i> Siz javoblarni ko'rishingiz uchun ro'yhatdan o'tishingiz va o'z profilingizga kirishingiz lozim !!! </p>
		</div>
	</div>
	@endif

</div>
@endsection('content')
@section('menyu')
@include('includes.mainmenyu')
@endsection('menyu')