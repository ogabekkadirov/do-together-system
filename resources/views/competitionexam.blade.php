@extends('layout.main')

@section('header')
@include('includes.header1')
@endsection('header')

@section('content')
<div class="mainnews">
	<div class="newscontent">
		<div class="nameexam">

			<div class="row">

				<div class="col-md-6">

					<p>
						<strong>
							<i>Test Nomi:</i>
						</strong>
						<span> {!! $utfc->test->name_test !!}</span>
					</p>
					<p>
						<strong>
							<i>Urinishlar soni:</i>
						</strong>

						@if($utfc->count_answer==0)
						<span> Javob yo'llamagansiz</span>
						@else
						<span> {!! $utfc->count_answer !!}  ta javob yo'llagansiz</span>
						<p>
							<strong>
								<i>Natija:</i>
								@if($utfc->resoult==1)
								<span class="true">To'g'ri</span>
								@else
								<span class="false">Noto'g'ri</span>
								@endif
							</strong>
						</p>
						@endif

					</p>	
				</div>
				<div class="col-md-6">
					<p>
						<strong>
							<i>Asl ball :</i>
						</strong>
						<span> {!! $utfc->test->ball !!} ball</span>
					</p>
					<p>
						<strong>
							<i>Joriy ball:</i>
						</strong>
						<span>{{$utfc->ball}} ball</span>
					</p>	
				</div>
			</div>
		</div>
		<div class="headexam">
			<span>Savolning berilishi</span>
		</div>
		<div class="exam">
			<span>
				{!! $utfc->test->exam !!}
			</span>
		</div>
		<div class="answer">
			<div class="headexam">
				<span>Variyantlar</span>
			</div>

			<div class="variants" style="border-left: 2px solid #3864ba;
			padding-left: 1em;	">
			<p>A )
				<span>{!! $utfc->test->a_answer !!}</span>
			</p>
			<p>B )
				<span>{!! $utfc->test->b_answer !!}</span>
			</p>
			<p>C ) 
				<span>{!! $utfc->test->c_answer !!}</span>
			</p>
			<p>D ) 
				<span>{!! $utfc->test->d_answer !!}</span>
			</p>
			@if(Session::has('fail'))
			<h6 style="color: red;font-size: 15px;"> {{ Session::get('fail') }} </h6>
			@endif	
		</div>
		<form action="{{ route('getanswercomp') }}" method="post">
			<div class="row">
				<div class="col-md-3">
				<input type="text" name="comp" value="{{ $utfc->competition_id }}" style="display: none;">
				</div>
				<div class="col-md-6">
					<div class="getanswer">
						<span>Javob : </span>
						<select name="answer">
							<option>A</option>
							<option>B</option>
							<option>C</option>
							<option>D</option>
						</select>
					</div>
					{!! csrf_field() !!}
					<input type="text" name="test" value="{{ $utfc->test->id }}" style="display: none;">
				</div>
				<div class="col-md-3">
					<div class="login">
						<button type="submit"> 
							<i class="fa fa-share-square"></i>Jo'natish
						</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
</div>

@endsection('content')

@section('menyu')
@include('includes.mainmenyu')
@endsection('menyu')
