@extends('layout.main')

@section('header')
@include('includes.header1')
@endsection('header')


@section('content')
<div class="mainnews">
	<div class="searchcontent">
		<div class="nameexam" style="margin-bottom: 2em;">

			<div class="row">

				<div class="col-md-6">

					<a href="{{ route('actionCompetitionTest',['message'=>$message]) }}">
						<strong>
							<i class="fa fa-list-ol">  Testlar</i>
						</strong>
					</a>	
				</div>
				<div class="col-md-6">
					<a href="{{ route('actionCompetitionresoult',['message'=>$message]) }}">
						<strong>
							<i class="fa fa-globe right" >  Hozirgi natijalar:</i>
						</strong>
					</a>	
				</div>
			</div>
		</div>
		<div class="retingsearch">
			<p>Bellashuvdagi barcha test savollariga yo'llangan javoblar jadvali</p>
			@if(Auth::check())
			<?php $name = Auth::user()->lastname.'  '.Auth::user()->firstname ?>
			<p><i class="fa fa-user-circle"></i><b>{{ $name }}</b> ning bellashuvdagi jo'natmalari</p>
			@endif
		</div>

	</div>
	<div class="table">
		<table class="table table-striped table-bordered table-hover ">
			<thead>
				<tr>
					<th> <i class="fa fa-list-ol"></i>№</th>
					<th><i class="fa fa-edit"></i> Test nomi</th>
					<th><i class="fa fa-calendar"></i> Vaqt</th>
					<th><i class="fa fa-bell"></i> Natifa</th>
				</tr>
			</thead>
			<tbody>
				<?php $number = 1; ?>
				@foreach($utfcs as $utfc)						   
				<tr>
					<td>{!! $number++ !!}</td>
					<td><a href="{{ route('actionCompetationexam',['id'=>$utfc->test->id,'comp'=>$comp]) }}">{!! $utfc->test->name_test !!}</a></td>
					<td>{{ $utfc->gettime }}</td>
					@if($utfc->resoult==1)
					<td class="true">To'g'ri</td>
					@else
					<td class="false">Noto'g'ri</td>
					@endif
				</tr>						    
				@endforeach
			</tbody>
		</table>
	</div>
	<div class="moreinfonumber">
		{!! $utfcs->links() !!}
	</div>
</div>	
@endsection('content')

@section('menyu')
@include('includes.mainmenyu')
@endsection('menyu')