@extends('layout.main')

<div class="container-fluid">
	<div class="headsubject">
		<div class="container">
			<div class="row">
				<div class="col-md-9 col-sm-12">
					<div class="row">
						<div class="col-md-6">
							<div class="logo">
								<a href="{{ route('actionIndex') }}"> <img src="{{ URL::to ('src/')}}/img/logo/innew.png"/>
								</a>
							</div>
							<div class="subjectname">
								<span>
									<strong>Kirish</strong>
								</span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="usersname">
								@if(Auth::check())
								<i class="fa fa-user"></i>
								<span>{{ Auth::user()->firstname }}  {{ Auth::user()->lastname}}</span>
								@endif
								@if(!Auth::check())
								<i class="fa fa-user"></i>
								<span>Siz xali profilingizga kirmagansiz </span>
								@endif
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="login">
						@if(Auth::check())
						<a href=" {{ route('logout')}}">
							<i class="fa fa-sign-out"></i>Chiqish
						</a>
						@endif
						@if(!Auth::check())
						<a href=" {{ route('login') }}">
							<i class="fa fa-sign-in"></i>Kirish
						</a>
						@endif
						
					</div>
				</div>	
			</div>

		</div>
		
	</div>
</div>

<div class="container-fluid">
	<div class="container">
		<div class="loginpanel">
			<div class="mainnews">
				<div class="inlogin">
					<div class="namelogin">
						<span><i class="fa fa-user"></i> Kirish</span>
					</div>
					<div class="inputinfo">
						<form action="{{ route('enter') }}" method="post">
							{!! csrf_field() !!}
							<!-- <label>Login</label> -->
							<div class="log">
								<input type="text" name="login" placeholder="Login" />
								<i class="fa fa-user"></i>
							</div>
							<!-- <label>Parol</label> -->
							<div class="log">
								<input type="password" name="password" placeholder="Parol" />
								<i class="fa fa-unlock-alt"></i>
							</div>
							<div class="postlogin tg-bannercontent1">
								<button class="tg-btn1" type="submit">
									<span>
										<i class="fa fa-sign-in"></i>
										Kirish
									</span>
								</button>
							</div>
							<div class="forgetpassw">										
								@if(Session::has('fail'))
								<h6 style="color: red;">{{ Session::get('fail') }}</h6>
								@endif
								<div class="row">
									<div class="col-md-6">
										<div class="tg-bannercontent">
											<a class="tg-btn" href="{{ route('registration') }}">
												<span>
													<i class="fa fa-registered"></i> Ro'yxatdan o'tish
												</span>
											</a>
										</div>
									</div>
									<div class="col-md-6">
										<div class="tg-bannercontent">
											<a class="tg-btn" href="javascript:void(0);(0);">
												<span>
													<i class="fa fa-unlock"></i> 
													Parolni unitdim
												</span>
											</a>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
