@extends('layout.main')

@section('header')
	@include('includes.header1')
@endsection('header')

@section('content')

<div class="mainnews">
	@if($status==1)
	<div class="searchcontent">
		<div class="onlinetestname">
			<span><i class="fa fa-globe"></i> Online olimpiada</span>
		</div>
			<?php $messagetime = App\Olimpiada::where('id',$id)->first()->begin_time;
		 $messageendtime = App\Olimpiada::where('id',$id)->first()->length;
		 $timenow = Carbon\Carbon::now();

			// $seconds = $messagetime->h*3600 + $messagetime->i*60 + $messagetime->s;
		?>
		<p style="display: none;" id="begintime">{{ $messagetime }}</p>
		<p style="display: none;" id="endtime">{{ $messageendtime }}</p>
		<p style="display: none;" id="timenow">{{ $timenow }}</p>
		<div class="nameexam">
			<div class="row">
				<div class="col-md-6">
					<p>
						<strong>
							<i>Test soni:</i>
						</strong>
						<span> {{$counttest}} ta</span>
					</p>
				</div>
				<div class="col-md-6">
					<p style="float: right;font-style: italic;">
						<a href="{{ route('actionResoultolimp',['id'=>$id]) }}">
							<i class="fa fa-users"></i> 
							<span>Hozirgi natijalar</span>
						</a>
					</p>
				</div>
			</div>
			<div class="timeolimpiada">
				<p>
					<span>
						<strong id="begin">Boshlandi </strong>
					</span>
					<input type="text" name="ok" style="display: none;" value="{{$m}}" id="m">
					<span>
						Qolgan vaqt :
						<b><i id="demo"></i></b>
					</span>
				</p>
			</div>
		</div>

	</div>
	<div class="table">
		<table class="table table-striped table-bordered table-hover ">
			<thead>
				<tr>
					<th> <i class="fa fa-list-ol"></i>№</th>
					<th> <i class="fa fa-edit"></i> Nomi </th>
					<th><i class="fa fa-book"></i> Mavzusi </th>
					<th><i class="fa fa-check-circle"></i>Ball</th>
					<th><i class="fa fa-users"></i>Yechganlar soni</th>
				</tr>
			</thead>
			<tbody>						   
				@foreach($tests as $test)
				<tr>
					<td>1</td>
					<td><a href="{{ route('actionTestexam',['id'=> $test->id ]) }}">{{$test->name_test}}</a></td>
					<td>{{ $test->theme }}</td>
					<td>{{ $test->ball }}</td>
					<td>{{ $test->true_answers}}</td>
				</tr>
				@endforeach						    
			</tbody>
		</table>
	</div>
	<div class="moreinfonumber">

	</div>
	@else
	<div class="searchcontent">
		<div class="onlinetestname">
			<span><i class="fa fa-globe"></i> Online olimpiada</span>
		</div>
		<div class="nameexam">
			<div class="row">
				<div class="col-md-6">
					<p>
						<strong>
							<i>Test soni:</i>
						</strong>
						<span> {{$counttest}} ta</span>
					</p>
				</div>
				<?php $b = Carbon\Carbon::now(); ?>
				<div class="col-md-6">
					@if($time<$b)
					<p style="float: right;font-style: italic;">
						<a href="{{ route('actionResoultolimp',['id'=>$id]) }}">
							<i class="fa fa-users"></i> 
							<span> Natijalar</span>
						</a>
					</p>
					@endif
				</div>
			</div>
			<div class="timeolimpiada">
				@if($time>$b)
				<p style="color: green;">
					{{ $messageFail }} <b>{{ $time }}</b> da boshlanadi !!!
				</p>
				@else
				<p style="color: red">
					{{ $messageFail }}<b>{{ $time }}</b> da bo'lib o'tgan !!!
				</p>
				@endif
			</div>
		</div>

	</div>
	@endif
</div>
@endsection('content')


@section('menyu')
@include('includes.mainmenyu')
@endsection('menyu')
@section('script')

<script type="text/javascript">

var myVar = setInterval(myTimer, 1000);
	var n = 0;
     $time = document.getElementById('demo');
// var sec = new Date();

// var hh = sec.getHours();
// var mm = sec.getMinutes();
// var ss = sec.getSeconds() 

// var timenow = hh*3600 + mm*60 + ss;
var time2 = document.getElementById('timenow').innerHTML;
// alert(time2);
var thisDateT2 = time2.substr(0, 10) + "T" + time2.substr(11, 8);
var jDate2 = new Date(thisDateT2);
var timenow = jDate2.getHours()*3600+jDate2.getMinutes()*60+jDate2.getSeconds();

var time1 = document.getElementById('begintime').innerHTML;
var thisDateT = time1.substr(0, 10) + "T" + time1.substr(11, 8);
var jDate = new Date(thisDateT);
var timebegin = jDate.getHours()*3600+jDate.getMinutes()*60+jDate.getSeconds();

var endtime = document.getElementById('endtime').innerHTML;
var thisDateT1 =  "2018-06-26T" + endtime.substr(0, 8);
var jDate1 = new Date(thisDateT1);
var endtime= jDate1.getHours()*3600+jDate1.getMinutes()*60+jDate1.getSeconds();
var time_left = endtime - (timenow-timebegin);
// alert(timebegin);
	function myTimer() {
		if(time_left<=0){
			$time.innerHTML="00:00:00 vaqt tugadi !!!";
			document.getElementById('begin').style.display = 'none';

    	
    	}
    	else{
    		time_left--;
        $time.innerHTML = Math.floor(time_left/3600) +':'+Math.floor((time_left%3600)/60)+':'+time_left%60;
    	}
    }

</script>
@endsection('script')
