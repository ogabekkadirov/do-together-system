@extends('layout.main')

@section('header')
@include('includes.header1')
@endsection('header')



@section('content')
<div class="mainnews">
	<div class="searchcontent">
		<div class="retingsearch">
			<p>Qatnashchilar retingi</p>
		</div>
		<div class="search">
			<div class="inputb">
			<form action="{{ route('searchUser') }}" method="post">		
				<div class="inputborder">
					<input type="text" name="searchuser" placeholder="Qidirayotganingizni kiriting....">
					<i class="fa fa-search"></i>
				</div>
				{!! csrf_field() !!}
				<button type="submit">Qidirish</button>
			</form>
			</div>
		</div>
	</div>
	<div class="table">
		<table class="table table-striped table-bordered table-hover ">
			<thead>
				<tr>
					<th> <i class="fa fa-trophy"></i> № </th>
					<th> <i class="fa fa-user"></i> Foydalanuvchi </th>
					<th><i class="fa fa-globe"></i>Viloyati</th>
					<th><i class="fa fa-calendar"></i> Hozir </th>
					<th><i class="fa fa-calendar"></i> Bellashish </th>
				</tr>
			</thead>
			<tbody>
				<?php $reting = ($users->currentPage()-1) * $users->perPage() + 1;  ?>

				@if(Auth::check())
				@foreach($users as $user)
				@if(Auth::user()->id==$user->id)
					<tr style="background-color: #B4EDFA;">		
						<td> {{ $reting++ }}</td>
						<td>
							<a href="{{ route('actionUserinfo',['id'=> $user->id,'reting'=>$reting-1]) }}">{!! $user->lastname !!}
								{!! $user->firstname !!}
							</a>
						</td>
						<td>{{ $user->country }}</td>
						<td>online</td>
						<td><i class="fa fa-user"></i></td>
					</tr>
				@elseif($user->isOnline())
					<tr>							
						<td> {{ $reting++ }}</td>
						<td>
							<a href="{{ route('actionUserinfo',['id'=> $user->id,'n'=>$reting-1]) }}">{!! $user->lastname !!}
								{!! $user->firstname !!}
							</a>
						</td>
						<td>{{ $user->country }}</td>
						<td style="color:green">online </td>
						<td>
							<div class="tg-bannercontent">
								<a class="tg-btn" href="{{ route('ActionUsertouser',['id'=>$user->id]) }}">
									<span>
										Taklif qilish
									</span>
								</a>
							</div>

						</td>
					</tr>
				@endif
				@endforeach
				@else
				@foreach($users as $user)
				@if($user->isOnline())
				<tr>
					<td> {{ $reting++ }}</td>
					<td>
						<a href="{{ route('actionUserinfo',['id'=> $user->id]) }}">{!! $user->lastname !!}
							{!! $user->firstname !!}
						</a>
					</td>
					<td>{{ $user->country }}</td>	
					<td>{!! $user->lasttime !!}</td>
				</tr>
				@endif
				@endforeach
				@endif
			</tbody>
		</table>
	</div>
	<div class="moreinfonumber">

			{!! $users->links() !!}

	</div>
</div>
@endsection('content')

@section('menyu')
@include('includes.mainmenyu')
@endsection('menyu')
