@extends('layout.main')

<div class="container-fluid">
	<div class="container">
		<div class="loginpanel">
			<div class="mainnews">
				<div class="inlogin">
					<div class="namelogin">
						<span><i class="fa fa-user"></i> Ro'yhatdan o'tish</span>
					</div>
					<div class="inputinfo">
						<form action="{{ route('getUser') }}" method="post">
							<!-- <label>Login</label> -->
							{!! csrf_field() !!}
							<div class="log">
								<input type="text" name="firstname" placeholder="Ismingiz" />
								<i class="fa fa-user"></i>
							</div>
							<div class="log">
								<input type="text" name="lastname" placeholder="Familyangiz" />
								<i class="fa fa-user"></i>
							</div>
							<div class="log">
								+9989
								<input type="text" name="tel_number" placeholder=""  style="width: 50%;font-size: 15px;" />
								<i class="fa fa-phone"></i>
							</div>
							<div class="log">
								<input type="text" name="login" placeholder="Loginingiz" />
								<i class="fa fa-sign-in"></i>
							</div>
							<div class="log">
								<select class="textage" name="country">
									<option>
										Tashkent shahar
									</option>
									<option>
										Toshkent viloyat
									</option>
									<option>
										Qoraqalpog'iston
									</option>
									<option>
										Xorazm	
									</option>
									<option>
										Surxondaryo 
									</option>
									<option>
										Qashqadaryo
									</option>
									<option>
										Navoiy	
									</option>
									<option>
										Buxoro	
									</option>
									<option>
										Samarqand	
									</option>

									<option>
										Jizzax 	
									</option>
									<option>
										Sirdaryo	
									</option>
									<option>
										Farg'ona	
									</option>
									<option>
										Namangan
									</option>
									<option>
										Andijon	
									</option>
								</select>
							</div>
							<div class="log">
								<select class="textage" name="type">
									<option>
										Maktab o'quvchisi
									</option>
									<option>
										Maktabni tugallagan
									</option>
								</select>
							</div>
							<div class="log">
								<input type="password" name="password" placeholder="Parolingiz" />
								<i class="fa fa-unlock-alt"></i>
							</div>
							<div class="log">
								<input type="password" name="conformpassword" placeholder="Parol tasdiqlash" style="width: 70%;" />
								<i class="fa fa-unlock"></i>
							</div>
							<div class="postlogin tg-bannercontent1">
								<button class="tg-btn1" type="submit">
									<span>
										<i class="fa fa-sign-in"></i>
										Yuborish
									</span>
								</button>
							</div>
							<div class="forgetpassw">	
								@if(Session::has('fail'))
								<h6 style="color: red;font-size: 13px;" id="error"> {{ Session::get('fail') }} </h6>
								@endif									
								<div class="row">
									<div class="col-md-6">
										<div class="tg-bannercontent">
											<a class="tg-btn" href="{{ route('bestlogin')}}">
												<span>
													<i class="fa fa-sign-out"></i> 
													kirish
												</span>
											</a>
										</div>
									</div>
									<div class="col-md-6">
										<!-- <div class="tg-bannercontent">
		 									<a class="tg-btn" href="javascript:void(0);(0);">
		 										<span>
		 											<i class="fa fa-unlock"></i> 
		 											Parolni tiklash
		 										</span>
		 									</a>
		 								</div> -->
		 							</div>
		 						</div>
		 					</div>
		 				</form>
		 			</div>
		 		</div>
		 	</div>
		 </div>
		</div>

	</div>
<script type="text/javascript">
	
	alert(document.getElementById('error').innerHTML);

</script>