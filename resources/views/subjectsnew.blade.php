@extends('layout.main')

@section('header')
@include('includes.header1')
@endsection('header')
@section('content')
<div class="mainnews">
	<div class="newscontent">
		@foreach($news as $new)
		<?php $id = $subject->id ; ?>
		<div class="news">
			<div class="namenews">
				<p><i class="fa fa-book"></i>{!! $new->theme !!}</p>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="imgnew">	
						<img src="{{ URL::to($new->image->path) }}"/>
					</div>			
				</div>
				<div class="col-md-8">
					<div class="info">
						
						<p>{!! str_limit($new->info,250) !!}</p>
						<div class="tg-bannercontent">
							<a class="tg-btn" href="{{ route('actionViewnew',['id'=>$new->id]) }}"><span>Ko'proq...</span></a>
						</div>
					</div>
					<div class="infonew">
						<ul style="color: #3864ba;font-weight: 500">
							<li><span><strong><i class="fa fa-user-circle"></i></strong> {!! $new->autor !!}</span></li>
							<li><span><strong></strong><i class="fa fa-calendar"></i>{!! $new->time !!}</span></li>
							<li><span><i class="fa fa-eye"></i>
								{!!$new->count_views!!} marta o'qilgan</span></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		@endforeach
	</div>
	<div class="moreinfonumber">
		{{ $news->links()}}
	</div>
</div>
@endsection('content')
@section('menyu')
@include('includes.mainmenyu')
@endsection('menyu')		