@extends('layout.main')

@section('header')
@include('includes.header1')
@endsection('header')

@section('content')

<div class="mainnews">
	<div class="searchcontent">
		<div class="retingsearch">
			<p>Barcha test savollari</p>
		</div>
		<div class="search">
			<div class="inputb">
				<form action="{{ route('searchTest') }}" method="post">
					<div class="inputborder">
						<input type="text" name="searchtest" placeholder="Test nomi yoki mavzuni kiriting....">
						<i class="fa fa-search"></i>
					</div>
					{!! csrf_field() !!}
					<button type="submit">Qidirish</button>
				</form>
			</div>
		</div>
	</div>
	<div class="table">
		<table class="table table-striped table-bordered table-hover ">
			<thead>
				<tr>
					<th> 
						<i class="fa fa-list-ol"></i>№
					</th>
					<th> 
						<i class="fa fa-edit"></i> Nomi 
					</th>
					<th>
						<i class="fa fa-book"></i> Mavzusi 
					</th>
					<th>
						<i class="fa fa-balance-scale"></i> Ball 
					</th>
					<th>
						<i class="fa fa-check-circle"></i> To'gri javoblar soni
					</th>
					<!-- <th><i class="fa fa-calendar"></i> Oxirgi jo'natma</th> -->
				</tr>
			</thead>
			<tbody>	
				<?php $number = ($tests->currentPage()-1) * $tests->perPage() + 1;  ?>
				@foreach($tests as $test)
				
							   	
				<tr>
					<td>{!! $number++ !!}</td>
					<td>
						<a href="{{ route('actionTestexam',['id'=> $test->id ]) }}" style="float: left;">
							{!! $test->name_test !!}
						</a>
						@if($test->have_answer)
							<i class="fa fa-check-circle" style="float: right;color: #62A1D8"></i>
						@endif
					</td>
					<td>{!! $test->theme !!}</td>
					<td>{!! $test->ball !!}</td>
					<td>{!! $test->true_answers !!}</td>
				</tr>
				@endforeach

			</tbody>
		</table>
	<div class="moreinfonumber">
		{!! $tests->links() !!}
	</div>
		<div class="searchcontent">
			<div class="retingsearch">
				<p ><i class="fa fa-check-circle" style="color: #62A1D8"></i> - Yechimi mavjud test savollari</p>
			</div>
		</div>
	</div>

</div>
@endsection('content')
@section('menyu')
@include('includes.mainmenyu')
@endsection('menyu')