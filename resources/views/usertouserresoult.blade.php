@extends('layout.main')

@section('header')
@include('includes.header1')
@endsection('header')



@section('content')
<div class="mainnews">
	<div class="searchcontent">
		<div class="retingsearch">
			<p>Qatnashchilar retingi</p>
		</div>
		<div class="search">
			<div class="inputb">
			<form action="{{ route('searchUser') }}" method="post">		
				<div class="inputborder">
					<input type="text" name="searchuser" placeholder="Qidirayotganingizni kiriting....">
					<i class="fa fa-search"></i>
				</div>
				{!! csrf_field() !!}
				<button type="submit">Qidirish</button>
			</form>
			</div>
		</div>
	</div>
	<div class="table">
		<table class="table table-striped table-bordered table-hover ">
			<thead>
				<tr>
					<th> <i class="fa fa-trophy"></i> № </th>
					<th> <i class="fa fa-user"></i> 1-Foydalanuvchi </th>
					<th><i class="fa fa-globe"></i>Ball</th>
					<th><i class="fa fa-envelope"></i>Ball</th>
					<th><i class="fa fa-check-circle"></i> 2 - Foydalanuvchi </th>
					<th><i class="fa fa-calendar"></i>Vaqt</th>
				</tr>
			</thead>
			<tbody>
				<?php $reting = ($ufbs->currentPage()-1) * $ufbs->perPage() + 1;  ?>
				@foreach($ufbs as $ufb)
					<tr>							
						<td> {{ $reting++ }}</td>
						<td>
							<a href="{{ route('actionUserinfo',['id'=> $ufb->user->id,'n'=>$reting-1]) }}">{!! $ufb->user->lastname !!}
								{!! $ufb->user->firstname !!}
							</a>
						</td>
						<td>{{ $ufb->user->country }}</td>
						<td>{{ $ufb->count_answer}}</td>
						<td>{!! $ufb->ball !!}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	<div class="moreinfonumber">

			{!! $ufbs->links() !!}

	</div>
</div>
@endsection('content')

@section('menyu')
@include('includes.mainmenyu')
@endsection('menyu')
