@extends('layout.main')

@section('header')
@include('includes.header1')
@endsection('header')
@section('content')
<div class="mainnews">
	<div class="newscontent">
		<div class="news">
			<div class="namenews">
				<p><i class="fa fa-book"></i>{!! $new->theme !!}</p>
			</div>
			<div class="row">
				<div class="newinfopage">
					<div class="imgnewinfo">	
						<img src="{{ URL::to($new->image->path) }}"/>
					</div>			
				</div>
				<div class="newinfopage">
					<div class="infopadding">
						<p>{!! $new->info !!}</p>
					</div>
					<div class="infonew">
						<ul style="color: #3864ba">
							<li><span><strong><i class="fa fa-user-circle"></i></strong> {!! $new->autor !!}</span></li>
							<li><span><strong></strong><i class="fa fa-calendar"></i>{!! $new->time !!}</span></li>
							<li><span><i class="fa fa-eye"></i>
								{!!$new->count_views!!}  marta o'qilgan</span></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="moreinfonumber">
			<div class="tg-bannercontent text-right">
				<a class="tg-btn" href="{{ route('actionSubject') }}">
					<span>
						<i class="fa fa-back"></i> Orqaga qaytish
					</span>
				</a>
			</div>
		</div>
	</div>
	@endsection('content')
	@section('menyu')
	@include('includes.mainmenyu')
	@endsection('menyu')		