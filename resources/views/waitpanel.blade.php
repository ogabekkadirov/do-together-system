@extends('layout.main')

@section('header')
@include('includes.header1')
@endsection('header')

@section('content')
<div class="mainnews">
	<div class="searchcontent">
		<div class="retingsearch">
			<p>So'rov jo'natildi kuting !!!</p>
		</div>
		<div class="search">
			<div class="inputb text-center">
				<span style="color: green"><i class="fa fa-hourglass-end"></i> Qolgan vaqt   <strong id="demo"></strong>  soniya <span id="tugadi" style="display: none;"> Ajratilgan vaqt tugadi !!!</span></span>
				@if(Session::has('fail'))
					<h5 style="color: red;font-size: 16px;" id="again"> {{ Session::get('fail') }} </h5>
				@endif
			</div>
			<input type="text" name="ok1aa" id="dd" style="display: none;" value="{{$message->id}}">
			<div class="retingsearch" id="okko1" style="display: none;">
				<p><i class="fa fa-thumbs-up">  So'rovingiz qabul qilindi !!! </i></p>
				<div class="inputb text-center right" >
					<a href="{{ route('actionCompetitionTest',['messgae'=>$message->id]) }}"><i class="fa fa-step-forward"></i> Boshlash</a>
				</div>
			</div>
			<div class="retingsearch" style="display: none;" id="okko2" >
				<p><i class="fa fa-thumbs-down" style="color: red;"> Sizning so'rovingiz rad qilindi !!! </i></p>
				<div class="inputb text-center right" >
					<a href="{{ route('ActionOnlineUsers') }}"><i class="fa fa-times"></i> Ortga </a>
				</div>
			</div>
			<div class="retingsearch" style="display: none;" id="okko4" >
				<p>
					<i class="fa fa-thumbs-down" style="color: red;"> Sizning so'rovingiz qabul qilinmadi.Qaytadan taklif qilish imkoniyatidan foydalanasizmi !!!</i>
				</p>
				<div class="inputb text-center right" >
					<a href="{{ route('actionCompetitionanswertrue',['message'=>$message->id]) }}"><i class="fa fa-step-forward"></i> Qayta taklif</a>
				</div>
				<div class="inputb text-center right" >
					<a href="{{ route('actionCompetitionanswerfalse',['message'=>$message->id]) }}"><i class="fa fa-times"></i> Bekor qilish</a>
				</div>
			</div>

			
		</div>


		<div class="retingsearch" id="eslatma">
			<p><i class="fa fa-question">  Eslatma agar so'rovingizga 120 soniya  ichida javob kelmasa u o'z o'zidan bekor qilinadi !!! </i></p>
			<p><i class="fa fa-question">  Kutish vaqtida javob kelmasdan turib boshqa oynaga o'tmang !!! </i></p>
			
		</div>
	</div>
</div>

@endsection('content')

@section('menyu')
@include('includes.mainmenyu')
@endsection('menyu')
@section('script')

<script>
	var myVar = setInterval(myTimer, 1000);
	var n = 0;
	 time_left = 120;
     $time = document.getElementById('demo');
	var d4 = document.getElementById('okko4');



	function myTimer() {
		var d = new Date();
		if(document.getElementById('demo').innerHTML == '0:0:0'){
    		   d4.style.display = 'block';
    		   document.getElementById('tugadi').style.display = 'block';
    		   document.getElementById('eslatma').style.display = 'none';
    		   document.getElementById('again').style.display = 'none';

    	}
    	else{
    	time_left--;
        $time.innerHTML = Math.floor(time_left/3600) +':'+Math.floor((time_left%3600)/60)+':'+time_left%60;
    	}
    
}
</script>

<script type="text/javascript">

	$(document).ready(function() {
		setInterval(function() {
			
ajaxRequest("GET","{{ route('getmg') }}",null,afterMessageSend);
		}, 5000);
	});

var array = new Array();
var jsonData;
var array_count=0;
// add market

function ajaxRequest(method, url, params, callback)
      {
        var http;
        if(window.XMLHttpRequest)
        {
          http = new XMLHttpRequest();
        } else
        {
          http = new ActiveXObject("Microsoft.XMLHTTP");
        }
        http.onreadystatechange = function(){
          if (http.readyState === XMLHttpRequest.DONE){
            var success = null
            if(http.status == 200)
              success = true;
            else
              success = false;
            callback(success,http.responseText);
          }
        }
        http.open(method, url, true);
        http.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
        http.setRequestHeader('X-Requested-With','XMLHttpRequest');
        http.send();
      }

function afterMessageSend(code, response){

var d = document.getElementById('dd').value;
var d1 = document.getElementById('okko1');
var d2 = document.getElementById('okko2');

var array = new Array();
var md;

 jsonData = JSON.parse(response);
var mg = jsonData.mg;
for (var i = 0; i < mg.length; i++) {
 	if(mg[i].id == d ){
 		md = mg[i].condition;
 	}

}
if(md == 'rad etildi'){
// alert('salom');
	d2.style.display = 'block';
	d1.style.display = 'none';
    document.getElementById('eslatma').style.display = 'none';
    document.getElementById('again').style.display = 'none';

    // document.getElementById('tugadi').style.display = 'none';

	


}
else
{
	if(md == 'qabul qilindi' && document.getElementById('tugadi').style.display == 'none' ){
		d1.style.display = 'block';
		d2.style.display = 'none';
    	document.getElementById('eslatma').style.display = 'none';
        document.getElementById('eslatma').style.display = 'again';

        // document.getElementById('tugadi').style.display = 'none';

		

	}
}
}
</script>
@endsection('script')	